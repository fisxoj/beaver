;;;; beaver.asd

(defsystem beaver
  :description "Describe beaver here"
  :author "Matt Novenstern <fisxoj@gmail.com>"
  :license  "Specify license here"
  :version (:read-file-form "version.sexp")
  :pathname "src"
  :components ((:file "beaver"))
  :depends-on ("alexandria"
               "dissect"
               "local-time")
  :homepage "https://fisxoj.github.io/beaver/"
  :in-order-to ((test-op (test-op beaver/test)))
  :long-description #.(uiop:read-file-string #P"README.rst"))

(defsystem beaver/st-json
  :description "JSON output using st-json"
  :depends-on ("beaver"
               "st-json")
  :pathname "src"
  :components ((:file "st-json")))

(defsystem beaver/ansi-term
  :description "Plain text output with ANSI color codes!"
  :depends-on ("beaver"
               "cl-ansi-text")
  :pathname "src"
  :components ((:file "ansi-term")))

(defsystem beaver/test
  :depends-on ("beaver"
               "rove")
  :pathname "t"
  :components ((:file "beaver"))
  :perform (test-op (op c)
                    (declare (ignore op))
                    (uiop:symbol-call :rove :run c)))
