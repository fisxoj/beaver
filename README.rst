.. image:: https://travis-ci.org/fisxoj/beaver.svg?branch=master
   :target: https://travis-ci.org/fisxoj/beaver
   :alt: Travis CI status badge
.. image:: https://coveralls.io/repos/github/fisxoj/beaver/badge.svg?branch=master
   :target: https://coveralls.io/github/fisxoj/beaver?branch=master
   :alt: Coveralls status badge
.. image:: https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg
   :alt: Contributor Covenant
   :target: CODE_OF_CONDUCT.md


:Source: `https://github.com/fisxoj/beaver <https://github.com/fisxoj/beaver>`_
:Docs:  `https://fisxoj.github.io/beaver/ <https://fisxoj.github.io/beaver/>`_

beaver is a common lisp library that's probably awesome.

-------
Example
-------
