(defpackage :beaver/ansi-term
  (:use :cl)
  (:local-nicknames (:colors :cl-ansi-text))
  (:export
   #:ansi-term-logger))

(in-package :beaver/ansi-term)

(defclass ansi-term-logger (beaver:logger)
  ((colors-enabled :initarg :colors-enabled
                   :reader colors-enabled-p
                   :initform t)))

(defparameter +package-color+ :blue)

(defparameter +timestamp-color+ :white)

(defparameter +message-color+ :white)

(defparameter +fields-color+ :cyan)

(defparameter +level-colors+ '(:crit   :magenta
                               :error  :red
                               :warn   :yellow
                               :info   :green
                               :debug  :white
                               :debug1 :white
                               :debug2 :white
                               :debug3 :white
                               :debug4 :white))

(defmethod beaver:format-message ((logger ansi-term-logger) package level (message string) format-args)
  (let ((colors:*enabled* (colors-enabled-p logger))
        (stream (beaver:stream-of logger)))
    (format stream "~&")
    (colors:with-color (+timestamp-color+ :stream stream)
      (local-time:format-timestring stream (local-time:now) :format beaver:+timestamp-format+))
    (princ #\[ stream)
    (colors:with-color ((getf +level-colors+ level) :stream stream)
      (format stream "~10a" level))
    (princ #\] stream)
    (colors:with-color (+package-color+ :stream stream)
      (format stream " ~(~a~): " (package-name package)))
    (colors:with-color (+message-color+ :stream stream)
      (apply #'format stream message format-args))
    (colors:with-color (+fields-color+ :stream stream)
      (format stream "~:{~%~t~(~a~)=~S~^ ~}" beaver:*fields*))))
