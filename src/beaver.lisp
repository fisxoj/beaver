(defpackage beaver
  (:use :cl)
  (:shadow #:error
           #:warn
           #:debug
           #:print)
  (:export
   #:+timestamp-format+
   #:*fields*
   #:*loggers*
   #:logger
   #:stream-of
   #:plain-logger
   #:with-backtrace
   #:with-fields
   #:crit
   #:error
   #:warn
   #:info
   #:debug
   #:debug1
   #:debug2
   #:debug3
   #:debug4
   #:package-log-level
   #:log-message
   #:format-message
   #:print))

(in-package :beaver)

(defvar *fields* nil
  "Dynamic variable for storing structured data.")

(defvar *package-log-levels* nil
  "Mapping of packages to desired log levels")

(defvar *log-levels* nil
  "Mapping of log level keywords to numerical level")

(defvar *loggers* nil
  "Instances of the logger class that define outputs to log to.")

(defparameter +default-log-level+ :info)

(defparameter +timestamp-format+
  local-time:+rfc3339-format+
  "Format for the timsetamp field, as used by local-time.")

(defun numerical-log-level (level)
  "Convert a level keyword to its numerical priority."
  (check-type level keyword)
  (getf *log-levels* level))

(defun package-log-level (package)
  (getf *package-log-levels*
        (typecase package
          ((or string symbol) (find-package package))
          (package package))
        +default-log-level+))

(defun numerical-package-log-level (package)
  (numerical-log-level (package-log-level package)))

(defun (setf package-log-level) (level package)
  (check-type level keyword)

  (setf
   (getf *package-log-levels*
         (typecase package
           ((or string symbol) (find-package package))
           (package package)))
   level))

(defclass logger ()
  ((stream :initarg :stream
           :initform (make-synonym-stream '*standard-output*)
           :accessor stream-of)))


(defclass plain-logger (logger)
  ())


(defmacro with-backtrace (condition &body body)
  (let ((condition-sym (gensym)))
    `(let ((,condition-sym ,condition))
       (with-fields (:condition-class (string-downcase (class-name (class-of ,condition-sym))) string
                      ;; Limit some long vectors, especially those fast-http uses
                     :backtrace
                     (let ((*print-length* 100))
                       (dissect:present condition nil))
                     string)
         ,@body))))


(defmacro with-fields ((&rest fields) &body body)
  (let ((values (loop :for (_ value __) :on fields :by #'cdddr :collecting (list (gensym) value))))
    `(let ,values
       (let ((*fields* (append (list ,@(loop :for (field _ type) :on fields :by #'cdddr
                                             :for value :in (mapcar #'first values)
                                             :collecting `(list ,field ,value ',type)))
                               *fields*)))
         ,@body))))


(defun do-log (package level message &rest format-args)
  (dolist (logger *loggers*)
    (log-message logger package level message format-args)))


(defgeneric log-message (logger package level message format-args)
  (:method :around (logger package level message format-args)
    (declare (ignore logger package message format-args))
    (when (>= (numerical-log-level level) (numerical-package-log-level package))
      (call-next-method)
      (terpri (stream-of logger))))

  (:method (logger package level message format-args)
    (format-message logger package level message format-args)))


(defgeneric format-message (logger package level message format-args)
  (:method ((logger plain-logger) package level (message string) format-args)
    (format (stream-of logger) "~&~a [~10a] ~(~a~): ~a~t~:{~(~a~)=~S~^ ~}"
            (local-time:format-timestring nil (local-time:now) :format +timestamp-format+)
            level
            (package-name package)
            (apply #'format nil message format-args)
            *fields*)))


(macrolet ((make-logger (name value)
             (check-type value (integer 0))
             (check-type name keyword)
             `(progn
                (defmacro ,(intern (string name)) (&rest args)
                  (loop :for (element . rest) :on args
                        :if (typep element '(or symbol cons))
                          :append (etypecase element
                                    (symbol (list (alexandria:make-keyword element) element t))
                                    (cons element))
                            :into fields
                        :else
                          :do (assert (stringp element) nil "Object after field definitions must be a format string.")
                              ;; once element isn't a cons or symbol, it should be a
                              ;; format string and any remaining elements are arguments.
                          :and :return `(with-fields ,fields
                                          (do-log ,*package* ,,name ,element ,@rest))))
                (setf (getf *log-levels* ,name) ,value))))
  (make-logger :crit   100)
  (make-logger :error  80)
  (make-logger :warn   70)
  (make-logger :info   60)
  (make-logger :debug  50)
  (make-logger :debug1 40)
  (make-logger :debug2 30)
  (make-logger :debug3 20)
  (make-logger :debug4 10))


(defmacro print (value)
  "Works like PRINT but logs like DEBUG."

  (let ((gensym (gensym "VALUE")))
    `(let ((,gensym ,value))
       (debug "~a" ,gensym)
       ,gensym)))
