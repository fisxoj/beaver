(defpackage :beaver/st-json
  (:use :cl)
  (:local-nicknames (:json :st-json))
  (:export
   #:st-json-logger))

(in-package #:beaver/st-json)

(defclass st-json-logger (beaver:logger)
  ())


(defmethod json:write-json-element ((element condition) stream)
  "A generic representation so condition classes can be passed as field.  Feel free to override for your own condition classes."
  (json:write-json-element (json:jso "class" (string-downcase (class-name (class-of element)))
                                     "message" (with-output-to-string (out)
                                                 (let ((*print-escape* nil))
                                                   (print-object element out))))
                           stream))


(defmethod json:write-json-element ((element standard-object) stream)
  (json:write-json-element (with-output-to-string (out) (print-object element out))
                           stream))


(defmethod beaver:format-message ((logger st-json-logger) package level (message string) format-args)
  (let ((stream (beaver:stream-of logger)))
    (format stream "~&")
    (write-char #\{ stream)
    (json:write-json-element "message" stream)
    (write-char #\: stream)
    (json:write-json-element (apply #'format nil message format-args) stream)
    (write-char #\, stream)
    (json:write-json-element "package" stream)
    (write-char #\: stream)
    (json:write-json-element (string-downcase (package-name package)) stream)
    (write-char #\, stream)
    (json:write-json-element "level" stream)
    (write-char #\: stream)
    (json:write-json-element (string-downcase level) stream)
    (write-char #\, stream)
    (json:write-json-element "timestamp" stream)
    (write-char #\: stream)
    (write-char #\" stream)
    (local-time:format-timestring stream (local-time:now) :format beaver:+timestamp-format+)
    (write-char #\" stream)
    (write-char #\, stream)

    ;; fields
    (json:write-json-element "fields" stream)
    (write-char #\: stream)
    (write-char #\{ stream)
    (loop :for (field value type) :in beaver:*fields*
          :for first := t :then nil
          :unless first :do (write-char #\, stream)
          :do (json:write-json-element (string-downcase field) stream)
          :do (write-char #\: stream)
          :do (if (eq type 'boolean)
                  (json:write-json-element (if value :true :false) stream)
                  (json:write-json-element value stream)))
    ;; end fields
    (write-char #\} stream)
    ;; end outer object
    (write-char #\} stream)))
